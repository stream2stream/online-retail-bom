/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v03;

/**
 *
 * @author Selvyn
 */
public class Item
{
    private double  item_cost;
    private int     quantity_ordered;
    private int     unique_id;
    private Product itsProduct;

    public Item(Product prod, double item_cost, int unique_id)
    {
        this.item_cost = item_cost;
        this.unique_id = unique_id;
        this.itsProduct = prod;
    }
    
    public  Item( Item old )
    {
        this.item_cost = old.item_cost;
        this.itsProduct = old.itsProduct;
        this.unique_id = old.unique_id;
    }
    
    public  Item()
    {
    }
    
    public  Item    copy()
    {
        return new Item( this );
    }
    
    public  Product getProduct()
    {
        return itsProduct;
    }
    
    public  double  getItemCost()
    {
        return item_cost;
    }
    
    public  int getQuantity()
    {
        return quantity_ordered;
    }
    
    public  int getUniqueId()
    {
        return unique_id;
    }
    
    public  double  getTotalCost()
    {
        return quantity_ordered * item_cost;
    }

    public void setItem_cost(double item_cost)
    {
        this.item_cost = item_cost;
    }

    public void setQuantity(int quantity)
    {
        this.quantity_ordered = quantity;
    }

    public void setUnique_id(int unique_id)
    {
        this.unique_id = unique_id;
    }
}
