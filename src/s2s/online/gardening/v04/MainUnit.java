/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v04;

import java.time.LocalDate;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
    public  static  void    main( String []args )
    {
        JSONFormatter jsonfmt = new JSONFormatter();
        XMLFormatter xmlfmt = new XMLFormatter();
        Account acc = new Account( null, 1 );
        
        ScreenWriter sw = Writer.getScreenWriter();
        
        sw.write(jsonfmt.format(acc));
        System.out.println("");
        
        sw.write(xmlfmt.format(acc));
        System.out.println("");

        Customer cc = new Customer("Selvyn", "Wright", LocalDate.of(1965, 5, 8), "Birmingham, UK");
        
        sw.write(jsonfmt.format(cc));
        
        System.out.println("");
    }
    
}
