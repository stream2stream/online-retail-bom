/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v04;

/**
 *
 * @author Selvyn
 */
public class JSONFormatter
{
    public  String  format( Account acc )
    {
        String result = "{\"Account\":[\n\t{\"Date Created\":\"" 
                + acc.getDateCreated().toString() + "\"}\n]}" ;
        
        return result;
    }
    
    public  String    format( Customer cc )
    {
        String result = "{\"Customer\":[\n\t{\"First Name\":\"" + cc.getFname() + "\"},";
        result += "\n\t{\"Last Name\":\"" + cc.getLname() + "\"},";
        result += "\n\t{\"Date of Birth\":\"" + cc.getDob().toString() + "\"},";
        result += "\n\t{\"Address\":\"" + cc.getAddress() + "\"}";
        result += "\n]}";
        
        return result;
    }

}
