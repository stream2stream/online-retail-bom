/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v06;

/**
 *
 * @author Selvyn
 */
public class JSONFormatter  extends StreamFormatter
{
    @Override
    public  String  format( Account acc )
    {
        reset();
        
        append("\"{Account\":[\n\t{\"Date Created\":\""); 
        append(acc.getDateCreated().toString());
        append("\"}\n]}");
        
        return getStreamAsString();
    }
    
    @Override
    public  String    format( Customer cc )
    {
        reset();
        
        append("\"Customer\":[\n\t{\"First Name\":\"");
        append(cc.getFname());
        append("\"},");
        append("\n\t{\"Last Name\":\"");
        append(cc.getLname());
        append("\"},");
        append("\n\t{\"Date of Birth\":\"");
        append(cc.getDob().toString());
        append("\"},");
        append("\n\t{\"Address\":\"");
        append(cc.getAddress());
        append("\"}");
        append("\n]}");
        
        return getStreamAsString();
    }

}
