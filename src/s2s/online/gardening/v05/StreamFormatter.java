/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v05;

/**
 *
 * @author Selvyn
 */
public class StreamFormatter
{    
    private StringBuilder   formattedStream = null;
    
    protected void    reset()
    {
        formattedStream = new StringBuilder();
    }
    
    protected void    append( String data )
    {
        formattedStream.append(data);
    }
    
    protected   String  getStreamAsString()
    {
        return formattedStream.toString();
    }
    
    public  String  format( Account acc ) throws Exception
    {
        throw new Exception("This methos should not be called");
    }

    public  String  format( Customer acc ) throws Exception
    {
        throw new Exception("This methos should not be called");
    }

}
