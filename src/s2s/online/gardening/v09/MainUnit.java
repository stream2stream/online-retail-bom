/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v09;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Selvyn
 */
public class MainUnit
{
    public static void main(String[] args)
    {
        MainUnit munit = new MainUnit();

        munit.bootstrap_products_items();
        munit.test_serialization_writer_all_products();
        munit.test_serialization_reader_all_products();

        munit.bootstrap_customers_accounts();
        munit.test_serialization_writer_all_customers();
        munit.test_serialization_reader_all_customers();

        System.out.println("=============================================================================");
        System.out.println("=============================================================================\n");
        
        munit.bootstrap_orders_accounts_items();
        munit.test_serialization_writer_all_objects();
        munit.test_serialization_reader_all_objects();
    }

    public void test_serialization_writer_all_products()
    {
        String fname = "c:\\tmp\\all-products.oos";

        // working with a Serializable File Writer
        SerializableFileWriter sfw = Writer.getSerializableFileWriter(fname);

        // Write the object out
        // HashMap are Serializable, so it can be written straight out
        // Because all objects in the HashMap are Serializable, they will all be
        // written out as well
        sfw.write( Warehouse.getInstance().getProducts());

        sfw.close();
    }

    public void test_serialization_reader_all_products()
    {
        String fname = "c:\\tmp\\all-products.oos";
        // Read the object from the filesystem
        InputChannel sfr = Reader.getSerializableFileReader(fname);
        sfr.openForReading(fname);

        // Read the entire object network back in
        HashMap< String, Product> productsIn = (HashMap< String, Product>) sfr.read();

        // Only process the objects if the read was successful
        if (productsIn != null)
        {
            // Iterate over all the Products...
            for (Map.Entry<String, Product> entry : productsIn.entrySet())
            {
                Product pp = entry.getValue();
                System.out.printf("Product ==> %s, %f, %s, %d\n", pp.getDescription(), pp.getPrice(), pp.getProductCode(), pp.getQuantity());

                // Iterate over all the Items within each Product
                for (Map.Entry<Integer, Item> itemEntry : pp.getItems().entrySet())
                {
                    Item anItem = itemEntry.getValue();
                    System.out.printf("Item ==> %f, %d\n", anItem.getItemCost(), anItem.getUniqueId());
                }
            }
        }
    }

    public void bootstrap_products_items()
    {
        // This call will bootstrap the products
        Warehouse.getInstance();
    }

    public void bootstrap_customers_accounts()
    {
        CustomerAccountMgr caMgr = CustomerAccountMgr.getInstance();
        // Create the accounts first and Customers
        Account acc = null;
        Customer cc = null;

        cc = new Customer("Sevlyn", "Wright", LocalDate.of(1965, 5, 8), "Somewhere in Birmingham");
        caMgr.addCustomer("C1", cc);
        acc = new Account(cc, 1);
        cc.setAccount(acc); // tie acc to the customer
        caMgr.addAccount(acc);

        cc = new Customer("Samuel", "Wright", LocalDate.of(1982, 4, 22), "Somewhere else in Birmingham");
        caMgr.addCustomer("C2", cc);
        acc = new Account(cc, 2);
        cc.setAccount(acc); // tie acc to the customer
        caMgr.addAccount(acc);

        cc = new Customer("Graham", "Meaden", LocalDate.of(1967, 4, 3), "Somewhere down South");
        caMgr.addCustomer("C3", cc);
        acc = new Account(cc, 3);
        cc.setAccount(acc); // tie acc to the customer
        caMgr.addAccount(acc);
    }

    public void test_serialization_writer_all_customers()
    {
        String fname = "c:\\tmp\\all-customers.oos";

        // working with a Serializable File Writer
        SerializableFileWriter sfw = Writer.getSerializableFileWriter(fname);

        // Write the object out
        // HashMap are Serializable, so it can be written straight out
        // Because all objects in the HashMap are Serializable, they will all be
        // written out as well
        sfw.write(CustomerAccountMgr.getInstance().getCustomers());

        // No need to write the accounts because they are linked to the customers
        sfw.close();
    }

    public void test_serialization_reader_all_customers()
    {
        String fname = "c:\\tmp\\all-customers.oos";
        // Read the object from the filesystem
        InputChannel sfr = Reader.getSerializableFileReader(fname);
        sfr.openForReading(fname);

        // Read the entire object network back in
        HashMap< String, Customer> customersIn = (HashMap< String, Customer>) sfr.read();

        // Only process the objects if the read was successful
        if (customersIn != null)
        {
            // Iterate over all the Customers...
            for (Map.Entry<String, Customer> entry : customersIn.entrySet())
            {
                Customer cc = entry.getValue();
                System.out.printf("Customer ==> %s, %s, %s, %s, %s\n",
                        entry.getKey(),
                        cc.getFname(),
                        cc.getLname(),
                        cc.getDob().toString(),
                        cc.getAddress());
                System.out.printf("\tAccount ==> %d, %s\n",
                        cc.getAccount().getAccountId(),
                        cc.getAccount().getDateCreated().toString());

            }
        }
    }

    /*
     * This method will use an Order object to link the Item and Product objects 
     * to the Customer and Account objects
     */
    public void bootstrap_orders_accounts_items()
    {
        // Get an account and a product
        Account acc = CustomerAccountMgr.getInstance().getAccount(1);
        Product pp = Warehouse.getInstance().getProduct("JDF-001");

        // only proceed if the keys were valid
        if (acc != null && pp != null)
        {
            try
            {
                // create new order giving it the order number of 1
                Order oo = new Order(acc, 1, LocalDate.now());

                // add the order the account
                acc.addOrder(oo);

                // Use the product to purchase X times the Product
                Item itm = pp.purchaseItem(3);

                // Only proceed if you used a valid item id
                if (itm != null)
                {
                    oo.addItem(itm);  // this call decrease the stock for this product
                }
            } catch (Exception ex)
            {
                Logger.getLogger(MainUnit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void test_serialization_writer_all_objects()
    {
        String fname = "c:\\tmp\\all-objects.oos";

        // working with a Serializable File Writer
        SerializableFileWriter sfw = Writer.getSerializableFileWriter(fname);

        // Write the object out
        // HashMap are Serializable, so it can be written straight out
        // Because all objects in the HashMap are Serializable, they will all be
        // written out as well
        sfw.write(CustomerAccountMgr.getInstance().getCustomers());

        sfw.close();
    }

    public void test_serialization_reader_all_objects()
    {
        String fname = "c:\\tmp\\all-objects.oos";
        // Read the object from the filesystem
        InputChannel sfr = Reader.getSerializableFileReader(fname);
        sfr.openForReading(fname);

        // Read the entire object network back in
        HashMap< String, Customer> customersIn = (HashMap< String, Customer>) sfr.read();

        // Only process the objects if the read was successful
        if (customersIn != null)
        {
            // Iterate over all the Customers...
            for (Map.Entry<String, Customer> entry : customersIn.entrySet())
            {
                Customer cc = entry.getValue();
                System.out.printf("Customer ==> %s, %s, %s, %s, %s\n",
                        entry.getKey(),
                        cc.getFname(),
                        cc.getLname(),
                        cc.getDob().toString(),
                        cc.getAddress());
                System.out.printf("\tAccount ==> %d, %s\n",
                        cc.getAccount().getAccountId(),
                        cc.getAccount().getDateCreated().toString());

                displayAccountOrderDetais(cc.getAccount());
            }
        }
    }

    public void displayAccountOrderDetais(Account acc)
    {
        HashMap< Integer, Order> orders = acc.getOrders();
        for (Map.Entry<Integer, Order> entry : orders.entrySet())
        {
            Order ord = entry.getValue();
            System.out.printf("Order ==> %d, %f, %s\n",
                    ord.getOrderNo(),
                    ord.getTotalCost(),
                    ord.getDateCreated().toString());

            displayOrderDetails(ord);
        }
    }

    public void displayOrderDetails(Order ord)
    {
        for (Item itm : ord.getItems())
        {
            System.out.printf("Item ==> %d, %f, %d\n",
                    itm.getUniqueId(),
                    itm.getItemCost(),
                    itm.getQuantity());
            System.out.printf("Product ==> %s has %d in stock\n",
                    itm.getProduct().getDescription(),
                    itm.getProduct().getQuantity());
        }
    }
}
