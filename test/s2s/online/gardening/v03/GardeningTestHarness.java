/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package s2s.online.gardening.v03;

import java.time.LocalDate;
import static org.junit.Assert.*;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Selvyn
 */
public class GardeningTestHarness
{
    private HashMap< String, Product > products;

    public GardeningTestHarness()
    {
    }

    @Before
    public void setUp()
    {
        products = new HashMap<>();
    }

    @Test
    public void test_account_customer_initialised_correctly()
    {
        // Setup
        Customer cc = new Customer();
        Account acc = new Account(cc, 1);

        // Execute
        cc.setAccount(acc);

        // Test
        assertNotNull("Account on Customer should NOT be NULL", cc.getAccount());
        assertNotNull("Customer on Account should NOT be NULL", acc.getCustomer());

        assertEquals("Customers should be the same", acc, acc.getCustomer().getAccount());
        assertEquals("Accounts should be the same", acc, cc.getAccount());
    }
    
    @Test
    public  void    test_product_catalogue_initialisation()
    {
        // Create a few Products
        products.put("JDF-001", new Product("JVC DVD Player", 179.99, "JDP-001", 5));
        products.put("SMA-001", new Product("Sony MIDI Amplifier", 203.45, "SMA-001", 3));
        products.put("LAIO-302", new Product("Lenovo All In One 302", 312.00, "LAIO-302", 7));

        // Create a few Items per product
        /*
         * Before these items can be created, you will need to modify the Item constructor
         * and add a field member that links back to its Product
         */
        for (Map.Entry<String, Product> entry : products.entrySet())
        {
            for (int idx = 0; idx < entry.getValue().getQuantity(); idx++)
            {
                Item anItem = new Item(entry.getValue(), entry.getValue().getPrice(), idx + 1);
                entry.getValue().addItem(anItem);
            }
        }
      
        assertTrue("Number of Items for product JDF-001", 
                products.get("JDF-001").getAllItems().size()==5);
        assertTrue("Number of Items for product SMA-001", 
                products.get("SMA-001").getAllItems().size()==3);
        assertTrue("Number of Items for product LAIO-302", 
                products.get("LAIO-302").getAllItems().size()==7);
    }
    
    @Test
    public  void    test_order_one_item_one_product()
    {
        Order ord = new Order(null, 1, LocalDate.now());
        
        String key = "JDF-001";
        Product pp = Warehouse.getInstance().getProduct(key);
        assertNotNull( "Product " + key + "does not exist", pp );
        
        Item itm = pp.getItem(0).copy();
        itm.setQuantity(1);
        ord.addItem(itm);
        
        assertTrue( "Total should match product price", (pp.getPrice()==ord.getTotalCost()));        
    }

    @Test
    public  void    test_order_one_item_two_product()
    {
        Order ord = new Order(null, 1, LocalDate.now());
        
        String key = "JDF-001";
        Product pp = Warehouse.getInstance().getProduct(key);
        assertNotNull( "Product " + key + "does not exist", pp );
        
        Item itm = pp.getItem(0).copy();
        itm.setQuantity(1);
        ord.addItem(itm);
        double expResult = pp.getPrice();

                
        key = "LAIO-302";
        pp = Warehouse.getInstance().getProduct(key);
        assertNotNull( "Product " + key + "does not exist", pp );
        
        itm = pp.getItem(0).copy();
        itm.setQuantity(1);
        ord.addItem(itm);
        
        expResult += pp.getPrice();

        assertTrue( "Total should match product price", (expResult==ord.getTotalCost()));        
    }

    @Test
    public  void    test_order_one_item_3x_two_product()
    {
        Order ord = new Order(null, 1, LocalDate.now());
        
        String key = "JDF-001";
        Product pp = Warehouse.getInstance().getProduct(key);
        assertNotNull( "Product " + key + "does not exist", pp );
        
        Item itm = pp.getItem(0).copy();
        itm.setQuantity(3);
        ord.addItem(itm);
        double expResult = pp.getPrice() * 3;

                
        key = "LAIO-302";
        pp = Warehouse.getInstance().getProduct(key);
        assertNotNull( "Product " + key + "does not exist", pp );
        
        itm = pp.getItem(0).copy();
        itm.setQuantity(1);
        ord.addItem(itm);
        
        expResult += pp.getPrice();

        assertTrue( "Total should match product price", (expResult==ord.getTotalCost()));        
    }
}
